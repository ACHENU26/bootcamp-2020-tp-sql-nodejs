/*--- --- --- REQUIRE --- --- --- */


const express = require('express')
const app = express()
const bodyParser = require("body-parser");
const mysql = require('mysql');
const axios = require('axios')

app.listen(3000)

app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))
/*--- --- --- CONNECTION DATABASE --- --- --- */

// Create the connection with required details
const con = mysql.createConnection({
    host, user, password, database,
});
con.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");
});

// Get the Host from Environment or use default
const host = 'localhost';

// Get the User for DB from Environment or use default
const user = 'root';

// Get the Password for DB from Environment or use default
const password = "password";


// Get the Database from Environment or use default
const database = 'TP_NODEJS';


con.connect(function (err) {
    if (err) throw err;
})

/* --- --- --- ROUTES --- --- --- */

/* CREATE 6 ROUTES
- ALL/
- POST/users
- GET/users/:user
- GET/users/:userid
- GET/users?limit=20&offset=0
- DELETE/users/:userld
- PUT/users/:userld
*/

/*---------------------------------------------------------------------------------------*/

// ALL method route
app.get('/', async function (req, res) {
    try {
        var sql = "SELECT * FROM user ";
        con.query(sql, function (err, result) {
            if (err) throw err;
            res.send(result);
        });
    } catch (error) {
        res.send(error)
    }
})

/*---------------------------------------------------------------------------------------*/

// POST method route // create a user 
app.post('/users', async function (req, res) {
    try {
        var pseudo = req.query.pseudo;
        var firstname = req.query.firstname;
        var lastname = req.query.lastname;
        var email = req.query.email;
        var date = new Date;
        var date = new Date;
        var values = [[pseudo, firstname, lastname, email, date]]
        if (pseudo && firstname && lastname && email) {
            await con.connect(function (err) {
                var sql = "INSERT INTO user (pseudo, firstname, lastname, email, creatAT, UpdateAT) VALUES ?";
                con.query(sql, [values, function (err, result) {
                    if (err) throw err;
                    res.send(result)
                }]);
            });
        } else {
            res.send(null);
        }


    } catch (error) {
        res.send(error)
    }
})

/*---------------------------------------------------------------------------------------*/

// GET method route // get a user
app.get('/users', async function (req, res) {
    try {
        con.connect(function (err) {
            var limit = req.query.limit;
            var offset = req.query.offset;
            var order = req.query.order;
            var reverse = req.query.reverse;
            var filter = '';
            if (order) {
                filter += 'ORDER BY ' + order;
                if (reverse == 1) {
                    filter += ' ASC '
                }
            }
            if (limit && offset) {
                filter += "LIMIT " + offset + ", " + limit
            }
            var sql = "SELECT * FROM user " + filter;
            con.query(sql, function (err, result) {
                if (err) throw err;
                res.send(result);
            });
        });
    } catch (error) {
        res.send(error)
    }
})

/*---------------------------------------------------------------------------------------*/

// GET method route // get a userid
app.get('/users/:userId', async function (req, res) {
    try {

        con.connect(function (err) {
            var id = req.params.userId
            var sql = "SELECT * FROM user WHERE id = ?";
            con.query(sql, [id], function (err, result) {
                if (err) throw err;
                res.status(200);
                res.send(result);
            });

        });

    } catch (error) {
        res.send(error)
    }
})

/*---------------------------------------------------------------------------------------*/

// GET method route // limit 20
/* app.get('/users?limit=20%offset=0', function (req, res) {


    res.send('/users');
})
*/

/*---------------------------------------------------------------------------------------*/

// DELETE method route // delete a userid
app.delete('/users/:userId', async function (req, res) {
    try {
        con.connect(function (err) {
            var id = req.params.userId
            var sql = "SELECT * FROM user WHERE id = ?";
            con.query(sql, [id], function (err, result) {
                if (err) throw err;
                res.status(200);
                res.send(result);
            });
            var sql = "DELETE FROM user WHERE id = ?";
            con.query(sql, [id], function (err, result) {
                if (err) throw err;
                res.status(400);
                res.send(result);
            });
        });
    } catch (error) {
        res.send(error)
    }
})

/*---------------------------------------------------------------------------------------*/

// PUT method route // Put (modify) a user id
app.put('/users/:usersId', function (req, res) {
    try {
        con.connect(function (err) {
            var id = req.params.userId;
            var pseudo = req.query.pseudo ? req.query.pseudo : 'PSEUDO';
            var firstname = req.query.firstname ? req.query.firstname : 'FIRSTNAME';
            var lastname = req.query.lastname ? req.query.lastname : 'LASTNAME';
            var email = req.query.email ? req.query.email : 'mail@mail.com';
            var date = new Date();
            var date = new Date();
            if (pseudo && firstname && lastname && email) {
                var sql = "UPDATE user SET pseudo = ?, firstname = ?, lastname = ?, email = ?, updatedAt = ? WHERE id = ?";
                con.query(sql, [pseudo, firstname, lastname, email, date, id], function (err, result) {
                    if (err) throw err;
                });

                var sql = "SELECT * FROM user WHERE id = ?";
                con.query(sql, [id], function (err, result) {
                    if (err) throw err;
                    console.log(result);
                    res.send(result);
                });
            } else {
                res.send(null);
            }
        });
    } catch (error) {
        res.send(error)
    }
});